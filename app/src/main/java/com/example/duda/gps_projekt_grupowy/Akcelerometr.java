package com.example.duda.gps_projekt_grupowy;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Gosia on 12/01/2017.
 */


public class Akcelerometr extends Activity implements SensorEventListener {

    protected TextView wyswietlacz;
    protected String PunktyZGPS;

    private static final int SHAKE_THRESHOLD = 600;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {

                    String tekst = wyswietlacz.getText().toString() + "\nWspolrzedne punktow pobranych z GPS\n" + PunktyZGPS;

                    writeToFile(tekst, this);
                    wyswietlacz.append("\nZapisano dane do pliku");

                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    protected void writeToFile(String data,Context context) {
        try {
            File myFile = new File("/sdcard/dane_z_gps.txt");
            myFile.createNewFile();

            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.close();
            fOut.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
