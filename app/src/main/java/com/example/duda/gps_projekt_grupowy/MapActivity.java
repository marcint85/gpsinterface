package com.example.duda.gps_projekt_grupowy;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by Marcin on 18.09.2016.
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latlng = null;
        Location locStart;
        Location locEnd;
        PolylineOptions polylineOptions;


        polylineOptions = new PolylineOptions();
        for(Location location: PointsList.getPointsList()) {
            latlng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.addMarker(markerOptions.position(latlng));
        }
        for(int i = 0; i < PointsList.getPointsList().size() - 1; i++) {
            locStart = PointsList.getPointsList().get(i);
            locEnd = PointsList.getPointsList().get(i + 1);
            polylineOptions.add(new LatLng(locStart.getLatitude(), locStart.getLongitude()), new LatLng(locEnd.getLatitude(), locEnd.getLongitude()))
                            .width(5)
                            .color(Color.RED);
        }

        if(PointsList.isClosePolyline()) {
            locStart = PointsList.getPointsList().get(0);
            locEnd = PointsList.getPointsList().get(PointsList.getPointsList().size() - 1);
            polylineOptions.add(new LatLng(locStart.getLatitude(), locStart.getLongitude()), new LatLng(locEnd.getLatitude(), locEnd.getLongitude()))
                    .width(5)
                    .color(Color.RED);
        }

        mMap.addPolyline(polylineOptions);

        if(latlng != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
        }
    }
}
