package com.example.duda.gps_projekt_grupowy;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Marcin on 24.01.2017.
 */
public class PunktProsta extends Akcelerometr {

    TextView wyswietlacz;
    ImageButton btnClose;
    ImageButton btnPomiar;
    ImageButton btnKasuj;
    ImageButton btnZapisz;
    private Location prostaStart = null;
    private Location prostaStop = null;
    private Location punkt = null;
    private LocationManager locManager;
    private LocationListener locListener;
    private Location mobileLocation;
    private Obliczenia obl;
    Location[] pointsArray = new Location[3];

    private ClipboardManager myClipboard;
    private ClipData myClip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pole);
        obl = new Obliczenia();
        wyswietlacz = (TextView) findViewById(R.id.wyswietlacz);
        wyswietlacz.setText("Prosze pobrac poczatek prostej");
        btnClose = (ImageButton) findViewById(R.id.exit);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        btnZapisz = (ImageButton) findViewById(R.id.zapiszdoschowka);
        btnZapisz.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void onClick(View v) {
                String text;
                text = wyswietlacz.getText().toString();
                myClip = ClipData.newPlainText("text", text);
                myClipboard.setPrimaryClip(myClip);
            }
        });
        PunktyZGPS = prepareAkcelerometrPoints();

        btnKasuj = (ImageButton) findViewById(R.id.next);
        disableImageButton(btnKasuj, R.drawable.reset_disabled);

        btnPomiar = (ImageButton) findViewById(R.id.zacznijPomiar);

        btnPomiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Location tmpPoint = buttonGetLocationClick();
                if(tmpPoint != null) {
                    if(prostaStart == null) {
                        prostaStart = tmpPoint;
                        wyswietlacz.setText("Pobrano poczatek prostej, pobierz koniec prostej");
                        prepareAkcelerometrPoints();
                        enableImageButton(btnKasuj, R.drawable.reset);
                    } else if (prostaStop == null){
                        if(tmpPoint.getLatitude() == prostaStart.getLatitude() && tmpPoint.getLongitude() == prostaStart.getLongitude()) {
                            wyswietlacz.setText("Pobrano taki sam punkt, wybierz inny by wyznaczyc prosta");
                        } else {
                            prostaStop = tmpPoint;
                            prepareAkcelerometrPoints();
                            wyswietlacz.setText("Pobrano koniec prostej, pobierz punkt do obliczenia");
                        }
                    } else {
                        punkt = tmpPoint;
                        prepareAkcelerometrPoints();
                        double wynik = obl.obliczProstaPunt(prostaStart, prostaStop, punkt);
                        String str = String.format("Odleglosc wynosi: %1$.2f m \nMozesz pobrac inny punkt", wynik);
                        wyswietlacz.setText(str);
                        //wyswietlacz.setText(("Odleglosc wynosi: " + Double.toString(wynik) + " , \nMozesz pobrac inny punkt"));
                    }
                }
                else
                    wyswietlacz.setText("Lokalizacja niedostępna\nSprobuj pobrac lokalizacje ponownie");

            }
        });
        btnKasuj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prostaStart = null;
                prostaStop = null;
                punkt = null;
                wyswietlacz.setText("Wykasowano dane, prosze pobrac poczatek prostej");

                disableImageButton(btnKasuj, R.drawable.reset_disabled);
            }
        });
    }

    private void getCurrentLocation() {
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locListener = new LocationListener() {
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onLocationChanged(Location location) {
                mobileLocation = location;
            }
        };
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
    }

    private Location buttonGetLocationClick() {
        getCurrentLocation(); // gets the current location and update mobileLocation variable
        if (mobileLocation != null) {
            locManager.removeUpdates(locListener); // This needs to stop getting the location data and save the battery power.
            wyswietlacz.setText("Pobierz nastepny punkt");
            return mobileLocation;
        } else {
            wyswietlacz.setText("Przepraszamy, lokalizacja niedokładna");
            return null;
        }
    }

    private void disableImageButton(ImageButton button, int imageResourceId) {
        button.setEnabled(false );
        button.setImageResource(imageResourceId);
    }

    private void enableImageButton(ImageButton button, int imageResourceId) {
        button.setEnabled(true );
        button.setImageResource(imageResourceId);
    }

    private String prepareAkcelerometrPoints() {

        pointsArray[0] = prostaStart;
        pointsArray[1] = prostaStop;
        pointsArray[2] = punkt;

        return pointsArray.toString();
    }
}
