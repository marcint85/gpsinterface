package com.example.duda.gps_projekt_grupowy;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import static java.lang.System.lineSeparator;

public class PomiarDrogi extends Akcelerometr {

    TextView wyswietlacz;
    ImageButton btnClose;
    ImageButton btnPomiar;
    ImageButton btnNext;
    ImageButton btnZapisz;
    private Location[] punkty;
    private int iloscPunktow;
    private LocationManager locManager;
    private LocationListener locListener;
    private Location mobileLocation;
    private Obliczenia obl;
    private ClipboardManager myClipboard;
    private ClipData myClip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pomiar_drogi);
        punkty = new Location[2];
        iloscPunktow = 0;
        obl = new Obliczenia();
        wyswietlacz = (TextView) findViewById(R.id.wyswietlacz);

        btnClose = (ImageButton) findViewById(R.id.exit);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        btnZapisz = (ImageButton) findViewById(R.id.zapiszdoschowka);
        btnZapisz.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void onClick(View v) {
                String text;
                if(iloscPunktow!=0){
                text = wyswietlacz.getText().toString() + "\nLista pobranych wspolrzednych:\nszerokosc, dlugosc";
                for(Location x: punkty){
                    text = text + "\n" + x.getLatitude() + " , " + x.getLongitude();
                }}
                else
                    text = wyswietlacz.getText().toString();
                myClip = ClipData.newPlainText("text", text);
                myClipboard.setPrimaryClip(myClip);
            }
        });

        PunktyZGPS = punkty.toString();

        btnNext = (ImageButton) findViewById(R.id.next);
        disableImageButton(btnNext, R.drawable.tick_disabled);

        btnPomiar = (ImageButton) findViewById(R.id.zacznijPomiar);

        btnPomiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(punkty[0] == null) {
                    PointsList.clearList();
                    PointsList.setClosePolyline(false);
                }

                if(punkty.length == iloscPunktow)
                {
                    punkty = powiekszTabPunkty(punkty);
                }

                punkty[iloscPunktow] = buttonGetLocationClick();
                if(punkty[iloscPunktow] != null) {
                    PointsList.addLocation(punkty[iloscPunktow]);
                    iloscPunktow++;
                    wyswietlacz.setText("Pobrano " + iloscPunktow +" punktow");
                }
                else
                    wyswietlacz.setText("Lokalizacja GPS niedostępna\nSprobuj pobrac pkt ponownie");

                if(iloscPunktow >= 2) {
                    enableImageButton(btnNext, R.drawable.tick);
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double wynik = obl.obliczDroge(punkty);
                String str = String.format("odległość to: %1$.2f m", wynik);
                wyswietlacz.setText(str);
                iloscPunktow = 0;
                punkty = new Location[2];
                disableImageButton(btnNext, R.drawable.tick_disabled);
            }
        });
    }

    private Location[] powiekszTabPunkty(Location[] tab){

        Location[] nowa = new Location[2*tab.length];
        for(int i = 0; i <tab.length; i++) {
            nowa[i] = tab[i];
        }
        return nowa;
    }

    private void getCurrentLocation() {
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locListener = new LocationListener() {
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onLocationChanged(Location location) {
                mobileLocation = location;
            }
        };
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
    }

    private Location buttonGetLocationClick() {
        getCurrentLocation(); // gets the current location and update mobileLocation variable
        if (mobileLocation != null) {
            locManager.removeUpdates(locListener); // This needs to stop getting the location data and save the battery power.
            wyswietlacz.setText("Pobierz nastepny punkt");
            return mobileLocation;
        } else {
            wyswietlacz.setText("Przepraszamy, lokalizacja niedokładna");
            return null;
        }
    }

    private void disableImageButton(ImageButton button, int imageResourceId) {
        button.setEnabled(false );
        button.setImageResource(imageResourceId);
    }

    private void enableImageButton(ImageButton button, int imageResourceId) {
        button.setEnabled(true );
        button.setImageResource(imageResourceId);
    }
}
