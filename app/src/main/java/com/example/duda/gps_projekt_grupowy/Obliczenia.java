package com.example.duda.gps_projekt_grupowy;

import android.location.Location;

import java.util.List;


/**
 * Created by Marcin on 2016-01-20.

 */
public class Obliczenia {

    public double deg2rad(double deg){
        return deg * (Math.PI/180);
    }

    public double obliczOdcinekWMetrach(Location start, Location stop){
        double R = 6371; // promien ziemi w km

        double resultDLat = deg2rad(stop.getLatitude() - start.getLatitude());
        double resultDLong = deg2rad(stop.getLongitude() - start.getLongitude());

        double a = Math.sin(resultDLat / 2) * Math.sin(resultDLat / 2) +
                Math.cos(deg2rad(start.getLatitude())) * Math.cos(deg2rad(stop.getLatitude())) *
                        Math.sin(resultDLong / 2) * Math.sin(resultDLong / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c * 1000 ; //odleglosc w metrach

        return d;
    }


    public double obliczOdcinek(Location start, Location stop){
        double resultLat = start.getLatitude() - stop.getLatitude();
        double resultLong = start.getLongitude() - stop.getLongitude();
        double resultAlt = start.getAltitude() - stop.getAltitude();

        double result = Math.pow(resultLat, 2) + Math.pow(resultLong,2) + Math.pow(resultAlt,2);
        result = Math.sqrt(result);
        if(result < 0)
            result = -result;
        return result;
    }

    public static double CalculatePolygonArea(Location coordinates[], int iloscPunktow)
    {
        double area = 0;

        if (iloscPunktow > 2)
        {
            for (int i = 0; i < iloscPunktow - 1; i++)
            {
                Location p1 = coordinates[i];
                Location p2 = coordinates[i + 1];
                area += (p2.getLongitude() - p1.getLongitude())
                        * (2 + Math.sin(ConvertToRadian(p1.getLatitude())) + Math.sin(ConvertToRadian(p2.getLatitude())));
            }

            area = area * 6378137 * 6378137 / 2;
        }

        return Math.abs(area);
    }

    private static double ConvertToRadian(double input)
    {
        return input * Math.PI / 180;
    }




    public double obliczPole(Location start[], int iloscPunktow){
        int n = iloscPunktow;
        double wynik = 0;
        Location current;
        Location prev;
        Location next;

        for(int i = 0; i < n; i++)
        {
            current = start[i];
            if(i == 0)
                prev = start[n-1];
            else
                prev = start[i-1];

            if(i == n-1)
                next = start[0];
            else
                next = start[i+1];

            // P = 0,5 |SUMA (Xi(Yi+1 * Yi-1))| lan = x, long = y
            wynik = wynik + current.getLatitude()*(next.getLongitude()-prev.getLongitude());
        }

        if(wynik < 0)
            wynik = -0.5 * wynik;
        else
            wynik = 0.5 * wynik;
        return wynik;
    }

    public double obliczDroge(Location start[]){

        double wynik = 0;
        for(int i = 0; i < start.length-2; i++)
        {
            wynik = wynik + obliczOdcinekWMetrach(start[i], start[i+1]);

        }
        if(wynik < 0)
            wynik = -wynik;

        return wynik;
    }

    public double obliczPion(Location start, Location stop){

        double odchylkaOdPionu = 0;
        if (start.getLongitude()!=stop.getLongitude() || start.getLatitude()!=stop.getLatitude())
        {
         odchylkaOdPionu = obliczOdcinekWMetrach(start, stop);
        }

        return odchylkaOdPionu;

    }


    public double obliczKat(Location loc1, Location loc2, Location loc3){

        double kat = 0;

        double a = obliczOdcinekWMetrach(loc1, loc2);
        double b = obliczOdcinekWMetrach(loc2, loc3);
        double c = obliczOdcinekWMetrach(loc1, loc3);

        kat = Math.acos(((a*a)+(b*b)-(c*c)) / (2*a*b));

        return kat;

    }

    public double obliczWysokosc(Location start, Location stop){

        double wysokosc = Math.abs(start.getAltitude()-stop.getAltitude());
        return wysokosc;
    }

    public double obliczProstaPunt(Location prostaStart, Location prostaStop, Location punkt) {

        double kat = obliczKat(prostaStart, prostaStop, punkt);
        double przeciwprostokatna = obliczOdcinekWMetrach(prostaStop, punkt);
        return Math.sin(kat) * przeciwprostokatna;
    }
}
