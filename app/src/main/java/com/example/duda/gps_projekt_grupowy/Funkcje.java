package com.example.duda.gps_projekt_grupowy;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.ImageButton;

public class Funkcje extends Activity {

    ImageButton btnOdcinek;
    ImageButton btnPole;
    ImageButton btnPomiarDrogi;
    ImageButton btnProwadz;
    ImageButton btnPunktProsta;
    ImageButton btnPomiarKata;
    ImageButton btnPomiarWysokosci;
    ImageButton btnPion;
    //test commit and push
    //test branch

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funkcje);

        btnOdcinek = (ImageButton)findViewById(R.id.odcinek_button);
        btnOdcinek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Funkcje.this, Odcinek.class));
            }
        });

        btnProwadz = (ImageButton)findViewById(R.id.prowadzdo_button);
        btnProwadz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( Funkcje.this, MapActivity.class));
            }
        });


        btnPole = (ImageButton)findViewById(R.id.powiezchnia_button);
        btnPole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Funkcje.this, Pole.class));
            }
        });

        btnPomiarDrogi = (ImageButton)findViewById(R.id.pomiardrogi_button);
        btnPomiarDrogi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Funkcje.this, PomiarDrogi.class));
            }
        });

        btnPunktProsta = (ImageButton)findViewById(R.id.pktodprostej_button);
        btnPunktProsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Funkcje.this, PunktProsta.class));
            }
        });

        btnPomiarKata = (ImageButton)findViewById(R.id.pomiar_kata_button);
        btnPomiarKata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Funkcje.this, PomiarKata.class));
            }
        });

        btnPomiarWysokosci = (ImageButton)findViewById(R.id.wysokosc_button);
        btnPomiarWysokosci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Funkcje.this, PomiarWysokosci.class));
            }
        });

        btnPion = (ImageButton)findViewById(R.id.pion_button);
        btnPion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Funkcje.this, Pion.class));
            }
        });
    }
}
