package com.example.duda.gps_projekt_grupowy;

import android.location.Location;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Marcin on 01.11.2016.
 */
public class PointsList {
    private static LinkedList<Location> pointsList = new LinkedList<>();
    private static boolean closePolyline;

    public static void addLocation(Location location) {
        pointsList.add(location);
    }

    public static List<Location> getPointsList() {
        return pointsList;
    }

    public static void clearList() {
        pointsList.clear();
    }

    public static boolean isClosePolyline() {
        return closePolyline;
    }

    public static void setClosePolyline(boolean closePolylineInput) {
        closePolyline = closePolylineInput;
    }
}
