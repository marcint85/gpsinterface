package com.example.duda.gps_projekt_grupowy;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.content.ClipData;
import android.content.ClipboardManager;
import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import android.util.Log;


public class Odcinek extends Akcelerometr {
    ImageButton btnClose;
    ImageButton btnPomiar;
    ImageButton btnZapisz;

    private LocationManager locManager;
    private LocationListener locListener;
    private Location mobileLocation;
    private Location loc1;
    private Location loc2;
    private Obliczenia obl;
    private SensorManager sensorManager;
    private Sensor accelerometrSensor;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 600;
    private int liczbaPunktow;

    private ClipboardManager myClipboard;
    private ClipData myClip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_odcinek);
        liczbaPunktow = 0;
        obl = new Obliczenia();


        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometrSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometrSensor, SensorManager.SENSOR_DELAY_GAME);

        wyswietlacz = (TextView) findViewById(R.id.wyswietlacz);

        btnClose = (ImageButton) findViewById(R.id.exit);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        btnZapisz = (ImageButton) findViewById(R.id.zapiszdoschowka);
        btnZapisz.setOnClickListener(new OnClickListener() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void onClick(View v) {
                String text;
            if(loc1!=null & loc2!=null){
                text = wyswietlacz.getText().toString() + "\nPobrane punkty to:" +
                        "\nszerokosc, dlugosc\n"+loc1.getLatitude()+" , "+loc1.getLongitude() + "\n" +
                        loc2.getLatitude()+" , "+loc2.getLongitude();
            }
            else
                text = wyswietlacz.getText().toString();

                myClip = ClipData.newPlainText("text", text);
                myClipboard.setPrimaryClip(myClip);
            }
        });

        btnPomiar = (ImageButton) findViewById(R.id.zacznijPomiar);
        btnPomiar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(liczbaPunktow == 0) {
                    PointsList.clearList();
                    PointsList.setClosePolyline(false);
                    loc1 = buttonGetLocationClick();
                    if (loc1 != null) {
                        liczbaPunktow++;
                        PointsList.addLocation(loc1);
                    } else {
                        wyswietlacz.setText("Lokalizacja niedostępna\nSprobuj pobrac pierwszy pkt");
                    }
                } else {
                    loc2 = buttonGetLocationClick();
                    if (loc2 != null) {
                        liczbaPunktow++;
                        PointsList.addLocation(loc2);
                        double wynik = obl.obliczOdcinekWMetrach(loc1,loc2);
                        PunktyZGPS = "\n"+loc1.getLatitude()+" , "+loc1.getLongitude()+"\n"+loc2.getLatitude()+" , "+loc2.getLongitude();
                        String str = String.format("odległość to: %1$.2f m", wynik);
                        wyswietlacz.setText(str);
                        liczbaPunktow = 0;
                    } else {
                        wyswietlacz.setText("Lokalizacja niedostępna\nSprobuj pobrac drugi pkt");
                    }
                }
            }
        });
    }

    private void getCurrentLocation() {
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locListener = new LocationListener() {
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onLocationChanged(Location location) {
                mobileLocation = location;
            }
        };

        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
    }

    private Location buttonGetLocationClick() {
        getCurrentLocation(); // gets the current location and update mobileLocation variable

        if (mobileLocation != null) {
            locManager.removeUpdates(locListener); // This needs to stop getting the location data and save the battery power.
            wyswietlacz.setText("Pobierz nastepny punkt");
            return mobileLocation;
        } else {
            wyswietlacz.setText("Przepraszamy, lokalizacja niedokładna");
            return null;
        }
    }

}
